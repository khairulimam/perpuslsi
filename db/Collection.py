class Collection:
    def __init__(self, mongo):
        if self.__collection__ == None:
            raise Exception('Child must implement super cunstructor!')

    def all(self):
        return self.__collection__.find()

    def where(self, query, projection=None):
        return self.__collection__.find(filter=query, projection=projection)

    def find(self, query, projection=None):
        return self.__collection__.find_one(filter=query)

    def delete(self, query):
        return self.__collection__.delete_one(query)

    def insert(self, query):
        return self.__collection__.insert_one(query)

    def update(self, query, data):
        return self.__collection__.find_one_and_update(filter=query, update=data)
