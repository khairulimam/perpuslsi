from bs4 import BeautifulSoup
import imghdr

def extractErrorMessagesFromForm(*args):
    messages = list()
    for form in args:
        for field, errors in form.errors.items():
            for error in errors:
                messages.append(
                    u"%s" % error)
    return messages

def clean_html(text):
    html = BeautifulSoup(text)
    return html.get_text()

def get_ext_from_img_header(file):
    return imghdr.what('unused', file)