from wtforms import Form, validators, StringField, FileField, TextAreaField, HiddenField
from customvalidator.isimage import IsImage


class AddBookForm(Form):
    judul = StringField('judul', [validators.length(min=5, message='Panjang judul minimal 5 karakter')])
    penulis = StringField('penulis', [validators.length(min=5, message='Panjang nama minimal 5 karakter')])
    tahunTerbit = StringField('tahunTerbit',
                              [validators.length(min=4, max=4, message="Tahun harus berupa angka dengan format YYYY")])
    kodeRak = StringField('kodeRak', [validators.DataRequired(message='Kode rak harus diisi!')])
    keterangan = TextAreaField('keterangan',
                               [validators.length(min=20, message='Keterangan buku harus diisi minimal 20 karakter')])


class ImageForm(Form):
    cover = FileField('cover', [IsImage('Cover harus berupa gambar', IsImage.NOT_REQUIRED)])


class EditBookForm(Form):
    judul = StringField('judul', [validators.length(min=5, message='Panjang judul minimal 5 karakter')])
    penulis = StringField('penulis', [validators.length(min=5, message='Panjang nama minimal 5 karakter')])
    tahunTerbit = StringField('tahunTerbit',
                              [validators.length(min=4, max=4, message="Tahun harus berupa angka dengan format YYYY")])
    kodeRak = StringField('kodeRak', [validators.DataRequired(message='Kode rak harus diisi!')])
    keterangan = TextAreaField('keterangan',
                               [validators.length(min=20, message='Keterangan buku harus diisi minimal 20 karakter')])
    bookid = HiddenField('bookid', [validators.DataRequired(message='ID Buku harus terisi!')])
