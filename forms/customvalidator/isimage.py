from wtforms import ValidationError
from utils.utils import get_ext_from_img_header

class IsImage:
    REQUIRED = 1
    NOT_REQUIRED = 0

    def __init__(self, message, flag):
        self.message = message
        self.flags = flag

    def __call__(self, form, field):
        if self.flags and get_ext_from_img_header(field.data.read()) is None:
            raise ValidationError(self.message)