import os
import uuid
import shutil

import flask
from flask.ext.pymongo import PyMongo, ObjectId
from pymongo import ReturnDocument
from werkzeug.utils import secure_filename

from forms.book import AddBookForm, ImageForm, EditBookForm
from utils.utils import extractErrorMessagesFromForm, clean_html

# init app
app = flask.Flask('perpuslsi')
app.config['UPLOAD_FOLDER'] = 'static/books'
app.config['COVER_DEFAULT'] = 'cover.png'
mongo = PyMongo(app)


@app.route('/')
def index():
    return flask.render_template('index.html')


@app.route('/help')
def help():
    return flask.render_template('help.html')


@app.route('/tos')
def tos():
    return flask.render_template('tos.html')


# auth route
@app.route('/auth/dashboard')
def dashboard():
    return flask.render_template('auth/index.html')


# book routes
@app.route('/auth/books')
def books():
    books = mongo.db.books.find()
    return flask.render_template('auth/books/index.html', books=books)


@app.route('/auth/book/<bookid>')
def book(bookid):
    book = mongo.db.books.find_one({'_id': ObjectId(bookid)})
    return flask.render_template('auth/books/show.html', book=book)


@app.route('/auth/book/edit/<bookid>')
def editBook(bookid):
    try:
        book = mongo.db.books.find_one({'_id': ObjectId(bookid)})
        assert book
        return flask.render_template('auth/books/edit.html', book=book)
    except Exception:
        return flask.abort(404)


@app.route('/auth/book/update', methods=['POST'])
def updateBook():
    inputs = flask.request.form
    files = flask.request.files
    inputForm = EditBookForm(inputs)
    imageForm = ImageForm(files)
    if inputForm.validate() and imageForm.validate():
        bookid = ObjectId(inputForm.bookid.data)
        updateData = {'judul': inputForm.judul.data, 'penulis': inputForm.penulis.data,
                      'tahunTerbit': inputForm.tahunTerbit.data,
                      'kodeRak': inputForm.kodeRak.data, 'keterangan': inputForm.keterangan.data,
                      'keterangan_clean': clean_html(inputForm.keterangan.data)}
        try:
            book = mongo.db.books.find_one({'_id': bookid})
            assert book
            # return flask.jsonify(book=book)
        except Exception:
            return flask.abort(404)
        file = files['cover']
        cover = book['cover']
        existing_cover_name = os.path.join(app.config['UPLOAD_FOLDER'], cover)
        if file.filename:
            if not (cover == app.config['COVER_DEFAULT']):
                # rewrite the binary file
                from io import BytesIO
                with open(existing_cover_name, 'wb') as rewrite:
                    shutil.copyfileobj(BytesIO(imageForm.cover.data.read()), rewrite)
            elif cover == app.config['COVER_DEFAULT']:
                cover = ("{}.{}".format(uuid.uuid4(), file.filename.split('.')[-1]), app.config['COVER_DEFAULT'])[
                    not file.filename]
                # upload the file
                filename = secure_filename(cover)
                file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
                updateData['cover'] = filename
        try:
            update = mongo.db.books.find_one_and_update({'_id': bookid}, {
                '$set': updateData
            }, return_document=ReturnDocument.AFTER, projection={'_id': False})
            assert update
        except Exception:
            return flask.abort(500)
        resp = {'status': 1, 'message': 'Buku telah diperbarui', 'book': update}
    else:
        resp = {'status': 0, 'message': []}
        resp['message'].extend(extractErrorMessagesFromForm(inputForm, imageForm))
    return flask.jsonify(resp), 200


@app.route('/auth/books/add')
def addBook():
    return flask.render_template('auth/books/add.html')


@app.route('/auth/books/save', methods=['POST'])
def saveBook():
    inputs = flask.request.form
    files = flask.request.files
    inputForm = AddBookForm(inputs)
    imageForm = ImageForm(files)
    if inputForm.validate() and imageForm.validate():
        file = files['cover']
        cover = ("{}.{}".format(uuid.uuid4(), file.filename.split('.')[-1]), app.config['COVER_DEFAULT'])[
            not file.filename]
        # upload the file
        filename = secure_filename(cover)
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        newBook = {'judul': inputForm.judul.data, 'penulis': inputForm.penulis.data,
                   'tahunTerbit': inputForm.tahunTerbit.data,
                   'kodeRak': inputForm.kodeRak.data, 'keterangan': inputForm.keterangan.data, 'cover': cover,
                   'keterangan_clean': clean_html(inputForm.keterangan.data)}
        try:
            assert mongo.db.books.insert_one(newBook)
            resp = {'status': 1, 'message': 'Berhasil menambah buku'}
        except Exception:
            return flask.abort(500)
    else:
        resp = {'status': 0, 'message': []}
        resp['message'].extend(extractErrorMessagesFromForm(inputForm, imageForm))
    return flask.jsonify(resp), 200


@app.route('/auth/book/delete/<bookid>', methods=['DELETE'])
def deleteBook(bookid):
    try:
        assert mongo.db.books.delete_one({'_id': ObjectId(bookid)})
        return flask.jsonify(status=1, message='Buku telah terhapus')
    except Exception:
        return flask.abort(404)


@app.errorhandler(404)
def pageNotFound(e):
    return flask.render_template('404.html')


@app.route('/get_cover/<cover>')
def getCover(cover):
    filename = os.path.join(app.config['UPLOAD_FOLDER'], cover)
    return flask.send_file(filename, mimetype='image/%s' % filename.split('.')[-1])
